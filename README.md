## Description
Java - Spring Boot Soap API


```bash
mvn compile
```
To run the application 
```bash
mvn spring-boot:run
```

Default port : 8083
To change the port : [FileToOpen](./src/main/resources/application.properties)



Test the application on SOAP with the [wsdl](http://localhost:8084/project-web-service/ws/products.wsdl)
