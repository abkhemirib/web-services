package com.example.projectwebservice.file;


import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;


import io.spring.guides.gs_producing_web_service.Product;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class ProductRepository {
    private static final Map<String, Product> products = new HashMap<>();

    @PostConstruct
    public void initData() {
        Product p1 = new Product();
        p1.setName("First product");
        p1.setId(1);
        p1.setDescription("This is the description of the first product");

        products.put(p1.getName(), p1);

        Product p2 = new Product();
        p2.setName("Second product");
        p2.setId(2);
        p2.setDescription("This is the description of the second product");

        products.put(p2.getName(), p2);


        Product p3 = new Product();
        p3.setName("Third product");
        p3.setId(3);
        p3.setDescription("This is the description of the third product");

        products.put(p3.getName(), p3);
    }

    public Product findProduct(String name) {
        Assert.notNull(name, "The product's name must not be null");
        return products.get(name);
    }
}
